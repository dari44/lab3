package dawson;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    
    @Test
    public void echoTest(){
        assertEquals("testing echo method",App.echo(5), 5);
    }
    
    @Test
    public void oneMoreTest(){
        assertEquals("testing oneMore method", App.oneMore(5), 6);
    }
}
